# tidy-block-generalization-comparison

## Name
Tidy Block Environment

## Description
Simulates an abstraction of the tidying up problem as a reinforcement learning environment.  Can be configured to allow for different tidying tasks.  States  can be a set to a vector-state or language-state.  Actions can be set to discrete-actions or language-actions.  Designed to allow for testing generalization between state and action representations.

 In a given rollout of the environment, there is a variable number of boxes, each uniquely colored.  There is also a variable number of **big** and **small** colored blocks distributed among the boxes with each box having a limited capacity.  An action consists of an action verb (**lift** / **push**), a target block (specified by size and color), a source box (specified by color) and a destination box (specified by color).  Actions are valid if the action verb is appropriate for the chosen block, the chosen block exists in the source box and there is space in the destination box. A successful action results in a block being moved from one box to another.  Blocks of different sizes require different action verbs to move them: Large blocks must be **pushed** while small blocks must be **lifted**.  Sometimes actions are blocked due to box capacity.  For example, a small block can not be moved to a full box even if it is of the correct color until another block is moved out of the box.  Similarly, big blocks require twice the space of small blocks so there must be adequate space in the destination box. Ordering and position of blocks within boxes are not considered when testing if a move is valid. The environment is solved when all the blocks are moved into boxes of matching colors which returns a reward of 1. There is a small negative reward of -0.01 for each step to encourage finding the shortest path to a solution.

The environment always resets to an unsolved yet solvable state.  The colors used for the boxes and blocks can be fixed after resetting or they can be sampled from a set of 48 training colors.  There is also a test set of 39 held-out colors used for testing generalization.

## Visuals
![Example Rollout Visualized](env_example.svg)

## Installation
Tested using Python 3.7.11.

Install dependencies
```
pip install -r requirements.txt
```

Simply copy the module `tidyblock.py` to the project you want to use it in.

## Usage

To run tests:
```
pytest test_tidyblock.py
```

Example usage:

```
import tidyblock
from tidyblock import TidyBlock
env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                state_representation="vector", action_representation="discrete")
```

## License
MIT License

