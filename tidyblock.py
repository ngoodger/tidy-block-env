import torch
import numpy as np


class TidyBlock:

    verb_vocab = ["lift", "push"]
    block_size_vocab = ["small", "big"]
    train_colours_vocab = ["amaranth", "amber", "amethyst", "apricot", "aquamarine", "azure", "baby blue", "beige",
                     "brick red", "black", "blue", "blue-green", "blue-violet", "blush", "bronze", "brown",
                     "burgundy", "byzantium", "carmine", "cerise", "cerulean", "champagne", "chartreuse green",
                     "chocolate", "cobalt blue", "coffee", "copper", "coral", "crimson", "cyan", "desert sand",
                     "electric blue", "emerald", "erin", "gold", "gray", "green", "harlequin", "indigo", "ivory",
                     "jade", "jungle green", "lavender", "lemon", "lilac", "lime", "magenta", "magenta rose"]

    test_colours_vocab = ["maroon", "mauve", "navy blue", "ochre", "olive", "orange", "orange-red", "orchid", "peach",
                    "pear", "periwinkle", "persian blue", "pink", "plum", "prussian blue", "puce", "purple",
                    "raspberry", "red", "red-violet", "rose", "ruby", "salmon", "sangria", "sapphire", "scarlet",
                    "silver", "slate gray", "spring bud", "spring green", "tan", "taupe", "teal", "turquoise",
                    "ultramarine", "violet", "viridian", "white", "yellow"]

    other_vocab = ["empty", "There", "is", "a", "an", "one", "multiple", "block", "blocks",
                          "in", "the", "box", ".", "no", "SOS", "EOS", "move", "from", "to"]

    vocab_list = verb_vocab + block_size_vocab + train_colours_vocab + test_colours_vocab + other_vocab
    vocab = {word: i for i, word in enumerate(vocab_list)}

    def __init__(self, num_env, max_colours, min_colours=None, max_space_in_box=4, colour_sample_size=None,
                 block_sizes="small only",
                 state_representation="vector", action_representation="discrete",
                 shuffle_state=False, omit_redundant_facts=False, colourset="train", empty_block_prob=0.0,
                 device="cpu"):

        # Validate and store parameters
        assert max_colours <= len(self.train_colours_vocab), \
            f"colours of {max_colours} is too big. A maximum of {len(self.train_colours_vocab)} is supported"
        assert colour_sample_size is None or colour_sample_size <= len(self.train_colours_vocab), \
            f"A colour_sample_size of {colour_sample_size} is too big. Max of {len(self.train_colours_vocab)} is supported"
        assert colour_sample_size is None or colour_sample_size >= max_colours, \
            f"colour_sample size of {colour_sample_size} must be greater than or equal to max_colours of {max_colours}"
        assert block_sizes in ["small only", "big and small"], \
            f"block_sizes: '{block_sizes}' is not supported. Only 'small only' or 'big and small' is supported"
        assert state_representation in ["vector", "language"], \
            f"State representation {state_representation} not supported"
        assert action_representation in ["discrete", "language", "simple_language"], \
            f"Action representation {action_representation} not supported"
        assert not (state_representation == "vector" and shuffle_state), \
            f"Can not shuffle_state for vector state"
        assert not (state_representation == "vector" and omit_redundant_facts), \
            f"Can not omit_redundant_facts for vector state"
        assert (min_colours is None or (max_colours >= min_colours)), \
            f"max_colours must be greater than or equal to min_colours"
        assert (colourset in ["train", "test"]), "Only colourset train or test is supported."

        self.num_env = num_env
        self.max_colours = max_colours
        self.min_colours = min_colours if min_colours is not None else max_colours
        self.max_space_in_box = max_space_in_box
        self.colour_sample_size = colour_sample_size if colour_sample_size is not None else max_colours
        self.block_sizes = block_sizes
        self.state_representation = state_representation
        self.action_representation = action_representation
        self.shuffle_state = shuffle_state
        self.omit_redundant_facts = omit_redundant_facts
        self.colourset = colourset
        self.empty_block_prob = empty_block_prob
        self.device = device

        # Constants
        self.verb_vocab_start = 0
        self.verb_vocab_end = len(self.verb_vocab)
        self.block_size_vocab_start = len(self.verb_vocab)
        self.block_size_vocab_end = len(self.verb_vocab) + len(self.block_size_vocab)
        self.train_colour_start = len(self.verb_vocab) + len(self.block_size_vocab)
        self.test_colour_start = len(self.verb_vocab) + len(self.block_size_vocab) + len(self.train_colours_vocab)
        self.train_colour_end = len(self.verb_vocab) + len(self.block_size_vocab) + len(self.train_colours_vocab)
        self.test_colour_end = (len(self.verb_vocab) + len(self.block_size_vocab) +
                                len(self.train_colours_vocab) + len(self.test_colours_vocab))

        self.colour_start = self.train_colour_start if colourset == "train" else self.test_colour_start
        self.colour_end = self.train_colour_end if colourset == "train" else self.test_colour_end

        self.action_verbs = 2 if self.block_sizes == "big and small" else 1  # Lift / Push
        self.box_template = "There is an {box_colour} box .".split()

        if action_representation == "language":
            self.action_template = "{verb} the {block_size} {block_colour} block from the \
                                   {start_box_colour} box to the {end_box_colour} box .".split()
        if action_representation == "simple_language":
            self.action_template = "{verb} {block_size} {block_colour} \
                                   {start_box_colour} {end_box_colour}".split()
        if action_representation != "discrete":
            self.action_lookup = {word[1:-1]: i for i, word in enumerate(self.action_template) if
                                  word[0] == "{" and word[-1] == "}"}
        self.block_template = "There is {amount} {block_size} {block_colour} \
                              {block_s} in the {box_colour} box .".split()

        self.pc_generator = np.random.PCG64()
        self.random_generator = np.random.default_rng(self.pc_generator)

        self.max_block_size = 2 if self.block_sizes == "big and small" else 1

        # Calculate derived parameters
        self.max_possible_text_len = self.max_colours * len(
            self.box_template) + self.max_block_size * self.max_colours * self.max_colours * len(
            self.block_template) + 1

        self.discrete_action_space = (self.action_verbs * self.max_block_size * self.max_colours
                                      * self.max_colours * self.max_colours)

        self.train_colour_sample = (np.expand_dims(np.arange(self.train_colour_start,
                                                             min(self.train_colour_end,
                                                                 self.train_colour_start + self.colour_sample_size)), 0)
                                    .repeat(self.num_env, 0))
        self.test_colour_sample = (np.expand_dims(np.arange(self.test_colour_start,
                                                            min(self.test_colour_end,
                                                                self.test_colour_start + self.colour_sample_size)), 0)
                                   .repeat(self.num_env, 0))

        self.colour_sample = (np.expand_dims(np.arange(self.colour_start,
                                                         min(self.colour_end,
                                                             self.colour_start + self.colour_sample_size)), 0)
                                .repeat(self.num_env, 0))

        # Memory preallocation
        self.box_colours = torch.zeros([self.num_env, self.max_colours], dtype=torch.long, device=self.device)
        self.block_sizes = torch.zeros([self.num_env, self.max_colours, self.max_space_in_box], dtype=torch.long,
                                       device=self.device)
        self.block_colours = torch.zeros([self.num_env, self.max_colours, self.max_space_in_box], dtype=torch.long,
                                         device=self.device)
        self.min_unblocked_steps = torch.zeros(self.num_env, dtype=torch.long, device=self.device)
        self.steps_taken = torch.zeros(self.num_env, dtype=torch.long, device=self.device)
        if self.state_representation == "language":
            self.obsv = torch.zeros([self.max_possible_text_len, self.num_env], dtype=torch.long, device=self.device)
            self.token_idx = torch.zeros([self.num_env], dtype=torch.long, device=self.device)
        else:
            self.obsv = torch.zeros([self.num_env, self.max_colours + self.max_colours * self.max_space_in_box
                                     + self.max_colours * self.max_space_in_box],
                                    dtype=torch.long, device=self.device)

        self.env_idx = torch.arange(self.num_env, device=self.device)

    def reset(self):
        terminals = torch.ones([self.num_env], dtype=torch.bool, device=self.device)
        self._reset(terminals)
        return self.get_obsv()

    def get_obsv(self):
        if self.state_representation == "language":
            return self._get_language_obsv()
        else:
            return self._get_vector_obsv()

    def _reset(self, terminals):
        while terminals.any():
            new_box_colours, new_block_sizes, new_block_colours = self._generate_new_random_state()
            self.box_colours = torch.where(terminals.unsqueeze(1), new_box_colours, self.box_colours)
            self.block_sizes = torch.where(terminals.unsqueeze(1).unsqueeze(2), new_block_sizes, self.block_sizes)
            self.block_colours = torch.where(terminals.unsqueeze(1).unsqueeze(2), new_block_colours, self.block_colours)
            current_min_unblocked_steps = self.calc_min_unblocked_steps()
            self.min_unblocked_steps = torch.where(terminals, current_min_unblocked_steps, self.min_unblocked_steps)
            terminals = self.check_all_in_correct_box()
        self.steps_taken = torch.where(terminals, 0, self.steps_taken)

    def calc_min_unblocked_steps(self):
        return (~((self.box_colours.unsqueeze(2) == self.block_colours)
                  | (self.block_colours == self.vocab["empty"]))).sum(dim=2).sum(dim=1)

    def _generate_new_random_state(self):

        def remove_overflow_boxes(np_block_sizes, np_block_colours):
            cumulative_block_size_ok = np_block_sizes.cumsum(axis=2) > self.max_space_in_box
            return np.where(cumulative_block_size_ok, 0, np_block_sizes), np.where(cumulative_block_size_ok, 0,
                                                                                   np_block_colours)

        np_block_sizes = np.random.randint(1, self.max_block_size + 1,
                                           size=[self.num_env, self.max_colours, self.max_space_in_box])

        # Choose randomised colour set
        np_box_colours = self.random_generator.permuted(self.colour_sample, axis=1)[:, :self.max_colours]

        # Choose randomised number of boxes by masking invalid colours as 0.
        box_idx = np.expand_dims(np.arange(self.max_colours), 0)
        sample_box_count = np.random.randint(self.min_colours, self.max_colours + 1, size=[self.num_env, 1])
        valid_boxes = box_idx < sample_box_count
        np_box_colours = np.where(valid_boxes, np_box_colours, 0)

        np_block_colours = np_box_colours.repeat(self.max_space_in_box).reshape(
            [self.num_env, self.max_colours, self.max_space_in_box])
        np_block_sizes = np.where(np.expand_dims(valid_boxes, 2), np_block_sizes, 0)
        np_block_sizes, np_block_colours = remove_overflow_boxes(np_block_sizes, np_block_colours)

        # Shuffle blocks
        saved_state = self.pc_generator.state
        np_block_sizes = self.random_generator.permuted(np_block_sizes, axis=1)
        self.pc_generator.state = saved_state
        np_block_colours = self.random_generator.permuted(np_block_colours, axis=1)

        # Remove any blocks that may have been shuffled to a non-existant box.
        np_block_colours = np.where(np.expand_dims(valid_boxes, 2), np_block_colours, 0)
        np_block_sizes = np.where(np.expand_dims(valid_boxes, 2), np_block_sizes, 0)
        # Remove any additional blocks that may have been shuffled to overfill a box.
        np_block_sizes, np_block_colours = remove_overflow_boxes(np_block_sizes, np_block_colours)
        # Remove two blocks from randomly selected box to ensure problem is solvable.
        first_empty_idx = np.random.randint(self.max_colours, size=[self.num_env])
        np_block_sizes[np.arange(self.num_env), first_empty_idx, 0:2] = 0
        np_block_colours[np.arange(self.num_env), first_empty_idx, 0:2] = 0

        # Randomly remove additional blocks
        drop_blocks = (np.random.rand(self.num_env, self.max_colours, self.max_space_in_box)
                       < self.empty_block_prob)
        np_block_sizes = np.where(drop_blocks, 0, np_block_sizes)
        np_block_colours = np.where(drop_blocks, 0, np_block_colours)

        box_colours_values = torch.from_numpy(np_box_colours).long().to(self.device)
        block_sizes_values = torch.from_numpy(np_block_sizes).long().to(self.device)
        block_colours_values = torch.from_numpy(np_block_colours).long().to(self.device)

        box_colours = torch.where(box_colours_values == 0, self.vocab["empty"], box_colours_values)
        block_sizes = torch.where(block_sizes_values == 0, self.vocab["empty"],
                    torch.where(block_sizes_values == 1,
                                self.vocab["small"], torch.where(block_sizes_values == 2, self.vocab["big"], -1)))

        block_colours = torch.where(block_colours_values == 0, self.vocab["empty"], block_colours_values)
        return box_colours, block_sizes, block_colours

    def _get_vector_obsv(self):
        # Flatten and concatenate internal state.
        self.obsv = torch.cat([self.box_colours.reshape(self.num_env, -1),
                               self.block_sizes.reshape(self.num_env, -1),
                               self.block_colours.reshape(self.num_env, -1), ], dim=1)
        return self.obsv.permute(1, 0)

    def tokenize_str(self, condition, template, tokens):
        for word in template:
            if self.is_placeholder(word):
                token = tokens[word]
            else:
                token = self.vocab[word]
            self._maybe_set_obsv_and_increment_idx(condition=condition, new_token_value=token)

    def _maybe_set_obsv_and_increment_idx(self, condition, new_token_value):
        # output_val = torch.where(condition, new_token_value, self.vocab["EOS"]).unsqueeze(0)
        # self.obsv.scatter_(0, self.token_idx.unsqueeze(0), output_val)
        if type(new_token_value) == int:
            new_token_value = torch.tensor(new_token_value, device=condition.device).expand(1, self.num_env)
        else:
            new_token_value = new_token_value.unsqueeze(0)
        self.obsv.scatter_(0, self.token_idx.unsqueeze(0), new_token_value)
        self.token_idx = torch.where(condition, self.token_idx + 1, self.token_idx)

    """
  def _maybe_set_obsv_and_increment_idx(self, condition, new_token_value):
      #output_val = torch.where(condition, new_token_value, self.vocab["EOS"]).unsqueeze(0)
      #self.obsv.scatter_(0, self.token_idx.unsqueeze(0), output_val)
      if type(new_token_value) == int:
          self.obsv[self.token_idx, self.env_idx] = new_token_value
      else:
          self.obsv[self.token_idx] = new_token_value
      self.token_idx = torch.where(condition, self.token_idx + 1, self.token_idx)


  def _maybe_set_obsv_and_increment_idx(self, condition, new_token_value):
      output_val = torch.where(condition, new_token_value, self.vocab["EOS"]).unsqueeze(0)
      self.obsv.scatter_(0, self.token_idx.unsqueeze(0), output_val)
      self.token_idx = torch.where(condition, self.token_idx + 1, self.token_idx)
  """

    def state_to_str(self, tokens):
        assert len(tokens.shape) == 1, "tokens should be 1 dimensional found {len(tokens.shape)} dimensions."
        out = ""
        for token in tokens:
            token_text = self.vocab_list[token]
            if token_text == "EOS":
                break
            if token_text == ".":
                out += f"{token_text}\n"
            else:
                out += f" {token_text}"
        return out

    def str_to_action(self, action_str):
        action_words = action_str.split()
        assert (len(action_words) == len(self.action_template)), "Action str words must exactly match action_template"
        action = torch.zeros([len(self.action_template), 1], dtype=torch.long, device=self.device)
        for i, word in enumerate(action_words):
            action[i] = self.vocab[word]
        return action

    @staticmethod
    def is_placeholder(word):
        return word[0] == "{" and word[-1] == "}"

    def _get_language_obsv(self):

        # Reset token index for building observation
        self.token_idx[:] = 0
        self.obsv[:] = self.vocab["EOS"]
        tokens = {}

        np_boxes_batch = self.box_colours.to("cpu").numpy()
        if self.shuffle_state:
            np_boxes_batch = self.random_generator.permuted(np_boxes_batch, axis=1)
        boxes_batch = torch.from_numpy(np_boxes_batch).to(self.device).permute(1, 0)

        for box_colour in boxes_batch:
            add_observation = box_colour != self.vocab["empty"]
            tokens["{box_colour}"] = box_colour
            self.tokenize_str(condition=add_observation, template=self.box_template, tokens=tokens)

        np_boxes_batch = self.box_colours.to("cpu").numpy()
        # np_colours_batch = self.box_colours.to("cpu").numpy()
        # np_size_batch = self.block_sizes.to("cpu").numpy()

        np_colours_batch = self.box_colours.to("cpu").numpy()
        np_size_batch = np.linspace(range(self.vocab["small"], self.vocab["small"] + self.max_block_size),
                                    range(self.vocab["small"], self.vocab["small"] + self.max_block_size), self.num_env,
                                    dtype=np.int64)

        tokens = {}
        if self.shuffle_state:
            np_boxes_batch = self.random_generator.permuted(np_boxes_batch, axis=1)
            np_colours_batch = self.random_generator.permuted(np_colours_batch, axis=1)
            np_size_batch = self.random_generator.permuted(np_size_batch, axis=1)
        boxes_batch = torch.from_numpy(np_boxes_batch).to(self.device).permute(1, 0)
        colours_batch = torch.from_numpy(np_colours_batch).to(self.device).permute(1, 0)
        size_batch = torch.from_numpy(np_size_batch).to(self.device).permute(1, 0)
        for box_colour in boxes_batch:
            tokens["{box_colour}"] = box_colour
            for block_colour in colours_batch:
                tokens["{block_colour}"] = block_colour
                for block_size in size_batch:
                    tokens["{block_size}"] = block_size
                    count = ((self.block_colours == block_colour.unsqueeze(1).unsqueeze(1)) & (
                            self.block_sizes == block_size.unsqueeze(1).unsqueeze(1))).sum(dim=2)
                    count = torch.where((self.box_colours == box_colour.unsqueeze(1)), count, 0).sum(dim=1)
                    tokens["{amount}"] = torch.where(count == 0, self.vocab["no"],
                                                     torch.where(count == 1, self.vocab["one"], self.vocab["multiple"]))
                    tokens["{block_s}"] = torch.where(count > 1, self.vocab["blocks"], self.vocab["block"])
                    add_observation = ~(((count == 0) & (self.omit_redundant_facts))
                                        | (box_colour == self.vocab["empty"]) | (block_colour == self.vocab["empty"]))
                    self.prev_obsv = self.obsv.clone()
                    self.tokenize_str(condition=add_observation, template=self.block_template, tokens=tokens)

        self.obsv.scatter_(0, self.token_idx.unsqueeze(0), self.vocab["EOS"])
        return self.obsv

    def _select_language_action(self, action):
        action_verb = action[self.action_lookup["verb"]]
        action_block_size = action[self.action_lookup["block_size"]]
        action_block_colour = action[self.action_lookup["block_colour"]]
        action_start_box_colour = action[self.action_lookup["start_box_colour"]]
        action_end_box_colour = action[self.action_lookup["end_box_colour"]]
        return action_verb, action_block_size, action_block_colour, action_start_box_colour, action_end_box_colour

    def _select_discrete_action(self, action):
        action = action.squeeze(0) % self.discrete_action_space
        action_verb_idx = action % self.action_verbs
        action_block_size_idx = (action % (self.max_block_size * self.action_verbs)) // self.action_verbs
        action_block_colour_idx = ((action % (self.max_block_size * self.max_colours * self.action_verbs))
                                   // (self.max_block_size * self.action_verbs))
        action_start_box_colour_idx = ((action % (self.max_block_size * self.max_colours
                                                  * self.max_colours * self.action_verbs))
                                       // (self.max_block_size * self.max_colours * self.action_verbs))
        action_end_box_colour_idx = action // (self.max_block_size * self.max_colours
                                               * self.max_colours * self.action_verbs)

        action_verb = torch.where(action_verb_idx == 0, self.vocab["lift"], self.vocab["push"])
        action_block_size = torch.where(action_block_size_idx == 0, self.vocab["small"], self.vocab["big"])
        action_block_colour = self.box_colours[self.env_idx, action_block_colour_idx]
        action_start_box_colour = self.box_colours[self.env_idx, action_start_box_colour_idx]
        action_end_box_colour = self.box_colours[self.env_idx, action_end_box_colour_idx]
        return action_verb, action_block_size, action_block_colour, action_start_box_colour, action_end_box_colour

    def _select_action(self, action):
        if self.action_representation == "language" or self.action_representation == "simple_language":
            return self._select_language_action(action)
        else:
            return self._select_discrete_action(action)

    def check_all_in_correct_box(self):
        return ((self.block_colours == self.box_colours.unsqueeze(2))
                              | (self.block_colours == self.vocab["empty"])
                              | (self.block_sizes == self.vocab["empty"])
                              | (self.box_colours.unsqueeze(2) == self.vocab["empty"])).all(dim=2).all(dim=1)
    def step(self, action):

        (action_verb, action_block_size, action_block_colour, action_start_box_colour,
         action_end_box_colour) = self._select_action(action)

        # Get index of the corresponding start and end boxes.
        start_box_idx = (self.box_colours == action_start_box_colour.unsqueeze(1)).float().argmax(dim=1)
        end_box_idx = (self.box_colours == action_end_box_colour.unsqueeze(1)).float().argmax(dim=1)

        # Get subsets of state based on action boxes.
        start_boxes_sizes = self.block_sizes[self.env_idx, start_box_idx]
        end_boxes_sizes = self.block_sizes[self.env_idx, end_box_idx]
        start_boxes_colours = self.block_colours[self.env_idx, start_box_idx]
        end_boxes_colours = self.block_colours[self.env_idx, end_box_idx]

        # Check for valid block and get location in start box.
        block_size_verb_match = ((action_block_size == self.vocab["small"]) & (self.vocab["lift"] == action_verb)
                                 | (action_block_size == self.vocab["big"]) & (self.vocab["push"] == action_verb))
        block_size_match = start_boxes_sizes == action_block_size.unsqueeze(1)
        block_colour_match = start_boxes_colours == action_block_colour.unsqueeze(1)
        block_match = block_size_match & block_colour_match
        block_valid = block_match.any(dim=1)
        start_block_idx = torch.argmax(block_match.float(), dim=1)

        # Check for valid space in end box.
        end_box_occupied = torch.where(end_boxes_sizes == self.vocab["empty"], 0,
                    torch.where(end_boxes_sizes == self.vocab["small"], 1,
                                torch.where(end_boxes_sizes == self.vocab["big"], 2, 99)))
        action_block_size_value = torch.where(action_block_size == self.vocab["empty"], 0,
                    torch.where(action_block_size == self.vocab["small"], 1,
                                torch.where(action_block_size == self.vocab["big"], 2, 99)))
        end_box_space = (self.max_space_in_box - end_box_occupied.sum(dim=1)) >= action_block_size_value
        end_block_idx = (end_boxes_sizes == self.vocab["empty"]).float().argmax(dim=1)

        valid_action = (block_size_verb_match & block_valid & end_box_space & (action_block_size != self.vocab["empty"])
                        & (action_block_colour != self.vocab["empty"])
                        & (action_start_box_colour != self.vocab["empty"])
                        & (action_end_box_colour != self.vocab["empty"]))

        # Move delete valid blocks from start box
        self.block_sizes[self.env_idx,
                         start_box_idx,
                         start_block_idx] = torch.where(valid_action, torch.tensor(self.vocab["empty"], device=self.device),
                                                        self.block_sizes[self.env_idx, start_box_idx, start_block_idx])
        self.block_colours[self.env_idx,
                           start_box_idx,
                           start_block_idx] = torch.where(valid_action, torch.tensor(self.vocab["empty"], device=self.device),
                                                          self.block_colours[self.env_idx, start_box_idx, start_block_idx])

        # Add valid blocks to end box
        self.block_sizes[self.env_idx,
                         end_box_idx,
                         end_block_idx] = torch.where(valid_action, action_block_size,
                                                      self.block_sizes[self.env_idx, end_box_idx, end_block_idx])
        self.block_colours[self.env_idx,
                           end_box_idx,
                           end_block_idx] = torch.where(valid_action, action_block_colour,
                                                        self.block_colours[self.env_idx, end_box_idx, end_block_idx])

        all_in_correct_box = self.check_all_in_correct_box()
        reward = -0.01 + 1. * all_in_correct_box

        terminals = all_in_correct_box
        self.steps_taken += 1
        delta_min_unblocked_steps = self.steps_taken - self.min_unblocked_steps
        self._reset(terminals)

        # reset terminals
        return self.get_obsv(), reward, terminals, delta_min_unblocked_steps, valid_action
