import torch
from tidyblock import TidyBlock
import math


def set_known_state(env):
    vocab = TidyBlock.vocab
    env.block_sizes = torch.tensor([[[vocab["small"], vocab["big"], vocab["small"], vocab["empty"]],
                                     [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                     [vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]]],
                                    [[vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]],
                                     [vocab["big"], vocab["empty"], vocab["small"], vocab["empty"]],
                                     [vocab["big"], vocab["big"], vocab["empty"], vocab["empty"]]]])
    env.block_colours = torch.tensor([[[vocab["amethyst"], vocab["amethyst"], vocab["amaranth"], vocab["empty"]],
                                       [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                       [vocab["amber"], vocab["amaranth"], vocab["empty"], vocab["empty"]]],
                                      [[vocab["amber"], vocab["amber"], vocab["empty"], vocab["empty"]],
                                       [vocab["amethyst"], vocab["empty"], vocab["amaranth"], vocab["empty"]],
                                       [vocab["amaranth"], vocab["amethyst"], vocab["empty"], vocab["empty"]]]])
    env.box_colours = torch.tensor([[vocab["amber"], vocab["amaranth"], vocab["amethyst"]],
                                    [vocab["amethyst"], vocab["amber"], vocab["amaranth"]]])
    env.min_unblocked_steps = env.calc_min_unblocked_steps()
    env.steps_taken[:] = 0

    return env


def set_almost_done_state(env):
    vocab = TidyBlock.vocab
    env.block_sizes = torch.tensor([[[vocab["small"], vocab["big"], vocab["small"], vocab["empty"]],
                                     [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                     [vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]]],
                                    [[vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]],
                                     [vocab["big"], vocab["empty"], vocab["small"], vocab["empty"]],
                                     [vocab["big"], vocab["big"], vocab["empty"], vocab["empty"]]]])
    env.block_colours = torch.tensor([[[vocab["amethyst"], vocab["amethyst"], vocab["amaranth"], vocab["empty"]],
                                       [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                       [vocab["amber"], vocab["amaranth"], vocab["empty"], vocab["empty"]]],
                                      [[vocab["amethyst"], vocab["amber"], vocab["empty"], vocab["empty"]],
                                       [vocab["amber"], vocab["empty"], vocab["amber"], vocab["empty"]],
                                       [vocab["amaranth"], vocab["amaranth"], vocab["empty"], vocab["empty"]]]])
    env.box_colours = torch.tensor([[vocab["amber"], vocab["amaranth"], vocab["amethyst"]],
                                    [vocab["amethyst"], vocab["amber"], vocab["amaranth"]]])
    env.min_unblocked_steps = env.calc_min_unblocked_steps()
    env.steps_taken[:] = 0
    return env


def set_known_state_1(env):
    vocab = TidyBlock.vocab
    env.block_sizes = torch.tensor([[[vocab["small"], vocab["big"], vocab["small"], vocab["empty"]],
                                     [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                     [vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]]]])
    env.block_colours = torch.tensor([[[vocab["amethyst"], vocab["amethyst"], vocab["amaranth"], vocab["empty"]],
                                       [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                       [vocab["amber"], vocab["amaranth"], vocab["empty"], vocab["empty"]]]])
    env.box_colours = torch.tensor([[vocab["amber"], vocab["amaranth"], vocab["amethyst"]]])
    env.min_unblocked_steps = env.calc_min_unblocked_steps()
    env.steps_taken[:] = 0
    return env

def set_empty_box_state(env):
    vocab = TidyBlock.vocab
    env.block_sizes = torch.tensor([[[vocab["small"], vocab["big"], vocab["small"], vocab["empty"]],
                                     [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                     [vocab["small"], vocab["small"], vocab["empty"], vocab["empty"]]]])
    env.block_colours = torch.tensor([[[vocab["amethyst"], vocab["amethyst"], vocab["amaranth"], vocab["empty"]],
                                       [vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"]],
                                       [vocab["amber"], vocab["amaranth"], vocab["empty"], vocab["empty"]]]])
    env.box_colours = torch.tensor([[vocab["amber"], vocab["empty"], vocab["amethyst"]]])
    env.min_unblocked_steps = env.calc_min_unblocked_steps()
    env.steps_taken[:] = 0
    return env


def test_vector_state():
    vocab = TidyBlock.vocab
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="discrete")
    env = set_known_state(env)
    print(len(env.vocab_list))
    obsv = env._get_vector_obsv()
    expected_obsv = torch.tensor([[vocab["amber"], vocab["amaranth"], vocab["amethyst"], vocab["small"], vocab["big"],
                                   vocab["small"], vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"],
                                   vocab["empty"], vocab["small"], vocab["small"], vocab["empty"], vocab["empty"],
                                   vocab["amethyst"], vocab["amethyst"], vocab["amaranth"],
                                   vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"], vocab["empty"],
                                   vocab["amber"], vocab["amaranth"], vocab["empty"], vocab["empty"]],
                                  [vocab["amethyst"], vocab["amber"], vocab["amaranth"], vocab["small"], vocab["small"],
                                   vocab["empty"], vocab["empty"], vocab["big"], vocab["empty"], vocab["small"],
                                   vocab["empty"], vocab["big"], vocab["big"], vocab["empty"], vocab["empty"],
                                   vocab["amber"], vocab["amber"], vocab["empty"],
                                   vocab["empty"], vocab["amethyst"], vocab["empty"], vocab["amaranth"], vocab["empty"],
                                   vocab["amaranth"], vocab["amethyst"], vocab["empty"], vocab["empty"]]])
    print(obsv == expected_obsv.permute(1, 0))
    assert (obsv == expected_obsv.permute(1, 0)).all()


def test_language_env_no_omissions():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="discrete")
    env = set_known_state(env)
    obsv = env._get_language_obsv()
    language_obsv = env.state_to_str(obsv[:, 1])
    assert (language_obsv ==
            " There is an amethyst box.\n"
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is no small amethyst block in the amethyst box.\n"
            " There is no big amethyst block in the amethyst box.\n"
            " There is multiple small amber blocks in the amethyst box.\n"
            " There is no big amber block in the amethyst box.\n"
            " There is no small amaranth block in the amethyst box.\n"
            " There is no big amaranth block in the amethyst box.\n"
            " There is no small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is no small amber block in the amber box.\n"
            " There is no big amber block in the amber box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is no big amaranth block in the amber box.\n"
            " There is no small amethyst block in the amaranth box.\n"
            " There is one big amethyst block in the amaranth box.\n"
            " There is no small amber block in the amaranth box.\n"
            " There is no big amber block in the amaranth box.\n"
            " There is no small amaranth block in the amaranth box.\n"
            " There is one big amaranth block in the amaranth box.\n")


def test_empty_box_not_in_state():
    env = TidyBlock(num_env=1, max_colours=3, min_colours=2, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="discrete")
    env = set_empty_box_state(env)
    obsv = env._get_language_obsv()
    language_obsv = env.state_to_str(obsv[:, 0])
    assert (language_obsv ==
            " There is an amber box.\n"
            " There is an amethyst box.\n"
            " There is no small amber block in the amber box.\n"
            " There is no big amber block in the amber box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is no big amber block in the amethyst box.\n"
            " There is no small amethyst block in the amethyst box.\n"
            " There is no big amethyst block in the amethyst box.\n")

def test_language_env_omit_redundant_facts():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="discrete", omit_redundant_facts=True)
    env = set_known_state(env)
    obsv = env._get_language_obsv()
    language_obsv_0, language_obsv_1 = env.state_to_str(obsv[:, 0]), env.state_to_str(obsv[:, 1])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")
    assert (language_obsv_1 ==
            " There is an amethyst box.\n"
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is multiple small amber blocks in the amethyst box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is one big amethyst block in the amaranth box.\n"
            " There is one big amaranth block in the amaranth box.\n")


def test_language_no_block_size_step_should_not_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # Both invalid
    start_obsv = env._get_language_obsv().clone()
    action_language_0 = "push the big amaranth block from the amber box to the amaranth box ."
    action_language_1 = "push the big amethyst block from the amaranth box to the amber box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    assert (obsv == start_obsv).all()
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False, False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_language_no_block_colour_step_should_not_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # Both invalid
    start_obsv = env._get_language_obsv().clone()
    action_language_0 = "lift the small amber block from the amber box to the amaranth box ."
    action_language_1 = "lift the big amethyst block from the amaranth box to the amethyst box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _= env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    assert (obsv == start_obsv).all()
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False, False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_language_wrong_verb_step_should_not_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # Both invalid
    start_obsv = env._get_language_obsv().clone()
    action_language_0 = "push the small amethyst block from the amber box to the amaranth box ."
    action_language_1 = "lift the big amethyst block from the amaranth box to the amethyst box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _= env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    # print(obsv == start_obsv)
    print(env.state_to_str(start_obsv[:, 0]))
    print(env.state_to_str(obsv[:, 0]))
    assert (obsv == start_obsv).all()
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False, False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_language_no_space_should_not_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # Both invalid
    start_obsv = env._get_language_obsv().clone()
    action_language_0 = "lift the small amethyst block from the amber box to the amber box ."
    action_language_1 = "push the big amethyst block from the amaranth box to the amaranth box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _= env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    assert (obsv == start_obsv).all()
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False, False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_language_valid_move_should_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # One valid one invalid move
    start_obsv = env._get_language_obsv().clone()

    language_obsv_0 = env.state_to_str(start_obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")

    action_language_0 = "lift the small amaranth block from the amber box to the amaranth box ."
    action_language_1 = "lift the small amaranth block from the amaranth box to the amaranth box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    assert (obsv[:, 1] == start_obsv[:, 1]).all()
    obsv = env._get_language_obsv()
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False, False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_language_valid_move_should_change_state():
    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state(env)

    # One valid one invalid move
    start_obsv = env._get_language_obsv().clone()

    language_obsv_0 = env.state_to_str(start_obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")

    action_language_0 = "lift the small amaranth block from the amber box to the amaranth box ."
    action_language_1 = "lift the small amaranth block from the amaranth box to the amaranth box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0),
                                                                             env.str_to_action(action_language_1)],
                                                                            dim=1))
    assert (obsv[:, 1] == start_obsv[:, 1]).all()
    obsv = env._get_language_obsv()
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01, -0.01])).all()
    assert (terminals == torch.tensor([False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()


def test_solving_env():
    env = TidyBlock(num_env=1, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="language", action_representation="language", omit_redundant_facts=True)
    env = set_known_state_1(env)

    # One valid one invalid move
    start_obsv = env._get_language_obsv().clone()
    language_obsv_0 = env.state_to_str(start_obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amaranth block in the amber box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")

    action_language_0 = "lift the small amaranth block from the amber box to the amaranth box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0)],
                                                                            dim=1))
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amethyst block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01])).all()
    assert (terminals == torch.tensor([False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-4, -4])).all()

    action_language_0 = "lift the small amethyst block from the amber box to the amethyst box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0)],
                                                                            dim=1))

    print(env.block_colours)
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amber block in the amethyst box.\n"
            " There is one small amaranth block in the amethyst box.\n"
            " There is one small amethyst block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01])).all()
    assert (terminals == torch.tensor([False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-3, -3])).all()

    action_language_0 = "lift the small amber block from the amethyst box to the amber box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(torch.cat([env.str_to_action(action_language_0)],
                                                                            dim=1))
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amber block in the amber box.\n"
            " There is one big amethyst block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amaranth block in the amethyst box.\n"
            " There is one small amethyst block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01])).all()
    assert (terminals == torch.tensor([False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-2, -2])).all()

    action_language_0 = "push the big amethyst block from the amber box to the amethyst box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _= env.step(torch.cat([env.str_to_action(action_language_0)],
                                                                            dim=1))


    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amber block in the amber box.\n"
            " There is one small amaranth block in the amaranth box.\n"
            " There is one small amaranth block in the amethyst box.\n"
            " There is one small amethyst block in the amethyst box.\n"
            " There is one big amethyst block in the amethyst box.\n")
    assert torch.isclose(reward, torch.tensor([-0.01])).all()
    assert (terminals == torch.tensor([False], dtype=torch.bool)).all()
    assert (delta_min_unblocked_steps == torch.tensor([-1, -1])).all()

    action_language_0 = "lift the small amaranth block from the amethyst box to the amaranth box ."
    obsv, reward, terminals, delta_min_unblocked_steps, _= env.step(torch.cat([env.str_to_action(action_language_0)],
                                                                            dim=1))
    """
    language_obsv_0 = env.state_to_str(obsv[:, 0])
    assert (language_obsv_0 ==
            " There is an amber box.\n"
            " There is an amaranth box.\n"
            " There is an amethyst box.\n"
            " There is one small amber block in the amber box.\n"
            " There is one multiple amaranth block in the amaranth box.\n"
            " There is one small amethyst block in the amethyst box.\n"
            " There is one big amethyst block in the amethyst box.\n")
    """
    assert torch.isclose(reward, torch.tensor([0.99])).all()
    assert (terminals == torch.tensor([True], dtype=torch.bool)).all()


def test_reset_env():
    vocab = TidyBlock.vocab
    RUN_COUNT = 10000
    OBSV_LEN = 27

    env = TidyBlock(num_env=1, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="language")

    obsv_values, obsv_prob = [], []
    for i in range(OBSV_LEN): obsv_values.append({})

    for j in range(RUN_COUNT):
        obsv = env.reset().permute(1, 0)
        assert(not env.check_all_in_correct_box().any()), "Env should never reset to solved state."
        for i in range(OBSV_LEN):
            val = obsv[0, i].item()
            if val in obsv_values[i].keys():
                obsv_values[i][val] += 1
            else:
                obsv_values[i][val] = 1

    for obsv_value in obsv_values:
        obsv_prob.append({k: (v / sum(obsv_value.values())) for k, v in obsv_value.items()})

    # Box 1 colour
    assert (math.isclose(obsv_prob[0][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 2 colour
    assert (math.isclose(obsv_prob[1][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 3 colour
    assert (math.isclose(obsv_prob[2][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 1
    # 1st size slot
    assert (math.isclose(obsv_prob[3][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[4][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[5][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[6][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[6][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 2
    # 1st size slot
    assert (math.isclose(obsv_prob[7][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[8][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[9][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[10][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[10][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 3
    # 1st size slot
    assert (math.isclose(obsv_prob[11][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[12][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[13][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[14][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[14][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 1
    # 1st colour slot
    assert (math.isclose(obsv_prob[15][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[16][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[17][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[18][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 2
    # 1st colour slot
    assert (math.isclose(obsv_prob[19][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[20][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[21][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[22][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 3
    # 1st colour slot
    assert (math.isclose(obsv_prob[23][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[24][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[25][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[26][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amethyst"]], 0.01, abs_tol=0.1))


def test_reset_single_env_in_batch():
    vocab = TidyBlock.vocab
    RUN_COUNT = 10000
    OBSV_LEN = 27

    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="language")
    env = set_almost_done_state(env)
    start_obsv = env._get_vector_obsv().clone().permute(1, 0)
    action_language_0 = "lift the small amber block from the amber box to the amaranth box ."
    action_language_1 = "lift the small amber block from the amethyst box to the amber box ."
    action = torch.cat([env.str_to_action(action_language_0), env.str_to_action(action_language_1)], dim=1)

    obsv_values, obsv_prob = [], []
    for i in range(OBSV_LEN): obsv_values.append({})

    for i in range(RUN_COUNT):
        env = set_almost_done_state(env)
        obsv, reward, terminals, delta_min_unblocked_steps, _ = env.step(action)
        obsv = obsv.permute(1, 0)
        for i in range(OBSV_LEN):
            val = obsv[1, i].item()
            if val in obsv_values[i].keys():
                obsv_values[i][val] += 1
            else:
                obsv_values[i][val] = 1
        assert torch.isclose(start_obsv[0], obsv[0]).all()
        assert torch.isclose(reward, torch.tensor([-0.01, 0.99])).all()
        assert (terminals == torch.tensor([False, True], dtype=torch.bool)).all()
        assert (delta_min_unblocked_steps == torch.tensor([-4, 0])).all()

    for obsv_value in obsv_values:
        obsv_prob.append({k: (v / sum(obsv_value.values())) for k, v in obsv_value.items()})

    # Box 1 colour
    assert (math.isclose(obsv_prob[0][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 2 colour
    assert (math.isclose(obsv_prob[1][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 3 colour
    assert (math.isclose(obsv_prob[2][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 1
    # 1st size slot
    assert (math.isclose(obsv_prob[3][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[4][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[5][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[6][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[6][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 2
    # 1st size slot
    assert (math.isclose(obsv_prob[7][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[8][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[9][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[10][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[10][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 3
    # 1st size slot
    assert (math.isclose(obsv_prob[11][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["big"]], 0.33, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[12][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["small"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["big"]], 0.33, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[13][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[14][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[14][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 1
    # 1st colour slot
    assert (math.isclose(obsv_prob[15][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[16][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[17][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[18][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 2
    # 1st colour slot
    assert (math.isclose(obsv_prob[19][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[20][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[21][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[22][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 3
    # 1st colour slot
    assert (math.isclose(obsv_prob[23][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[24][vocab["empty"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amaranth"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amber"]], 0.22, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amethyst"]], 0.22, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[25][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[26][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amethyst"]], 0.01, abs_tol=0.1))


def test_variable_problem_size():
    vocab = TidyBlock.vocab
    RUN_COUNT = 10000
    OBSV_LEN = 27

    env = TidyBlock(num_env=1, max_colours=3, min_colours=2, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="language")

    obsv_values, obsv_prob = [], []
    for i in range(OBSV_LEN): obsv_values.append({})

    for i in range(RUN_COUNT):
        obsv = env.reset().permute(1, 0)
        for i in range(OBSV_LEN):
            val = obsv[0, i].item()
            if val in obsv_values[i].keys():
                obsv_values[i][val] += 1
            else:
                obsv_values[i][val] = 1

    for obsv_value in obsv_values:
        obsv_prob.append({k: (v / sum(obsv_value.values())) for k, v in obsv_value.items()})

    # Box 1 colour
    assert (math.isclose(obsv_prob[0][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[0][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 2 colour
    assert (math.isclose(obsv_prob[1][vocab["amaranth"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amber"]], 0.33, abs_tol=0.1))
    assert (math.isclose(obsv_prob[1][vocab["amethyst"]], 0.33, abs_tol=0.1))

    # Box 3 colour
    assert (math.isclose(obsv_prob[2][vocab["empty"]], 0.52, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amaranth"]], 0.16, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amber"]], 0.16, abs_tol=0.1))
    assert (math.isclose(obsv_prob[2][vocab["amethyst"]], 0.16, abs_tol=0.1))

    # Box 1
    # 1st size slot
    assert (math.isclose(obsv_prob[3][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["small"]], 0.28, abs_tol=0.1))
    assert (math.isclose(obsv_prob[3][vocab["big"]], 0.28, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[4][vocab["empty"]], 0.45, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["small"]], 0.27, abs_tol=0.1))
    assert (math.isclose(obsv_prob[4][vocab["big"]], 0.27, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[5][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[5][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[6][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[6][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 2
    # 1st size slot
    assert (math.isclose(obsv_prob[7][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["small"]], 0.28, abs_tol=0.1))
    assert (math.isclose(obsv_prob[7][vocab["big"]], 0.28, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[8][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["small"]], 0.28, abs_tol=0.1))
    assert (math.isclose(obsv_prob[8][vocab["big"]], 0.28, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[9][vocab["empty"]], 0.64, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["small"]], 0.3, abs_tol=0.1))
    assert (math.isclose(obsv_prob[9][vocab["big"]], 0.05, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[10][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[10][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 3
    # 1st size slot
    assert (math.isclose(obsv_prob[11][vocab["empty"]], 0.66, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["small"]], 0.16, abs_tol=0.1))
    assert (math.isclose(obsv_prob[11][vocab["big"]], 0.16, abs_tol=0.1))

    # 2nd size slot
    assert (math.isclose(obsv_prob[12][vocab["empty"]], 0.66, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["small"]], 0.16, abs_tol=0.1))
    assert (math.isclose(obsv_prob[12][vocab["big"]], 0.16, abs_tol=0.1))

    # 3rd size slot
    assert (math.isclose(obsv_prob[13][vocab["empty"]], 0.82, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["small"]], 0.15, abs_tol=0.1))
    assert (math.isclose(obsv_prob[13][vocab["big"]], 0.02, abs_tol=0.1))

    # 4th size slot
    assert (math.isclose(obsv_prob[14][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[14][vocab["small"]], 0.04, abs_tol=0.1))

    # Box 1
    # 1st colour slot
    assert (math.isclose(obsv_prob[15][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amaranth"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amber"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[15][vocab["amethyst"]], 0.18, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[16][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amaranth"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amber"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[16][vocab["amethyst"]], 0.18, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[17][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[17][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[18][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[18][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 2
    # 1st colour slot
    assert (math.isclose(obsv_prob[19][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amaranth"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amber"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[19][vocab["amethyst"]], 0.18, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[20][vocab["empty"]], 0.44, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amaranth"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amber"]], 0.18, abs_tol=0.1))
    assert (math.isclose(obsv_prob[20][vocab["amethyst"]], 0.18, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[21][vocab["empty"]], 0.63, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amaranth"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amber"]], 0.12, abs_tol=0.1))
    assert (math.isclose(obsv_prob[21][vocab["amethyst"]], 0.12, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[22][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[22][vocab["amethyst"]], 0.01, abs_tol=0.1))

    # Box 3
    # 1st colour slot
    assert (math.isclose(obsv_prob[23][vocab["empty"]], 0.67, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amaranth"]], 0.14, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amber"]], 0.14, abs_tol=0.1))
    assert (math.isclose(obsv_prob[23][vocab["amethyst"]], 0.14, abs_tol=0.1))

    # 2nd colour slot
    assert (math.isclose(obsv_prob[24][vocab["empty"]], 0.67, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amaranth"]], 0.14, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amber"]], 0.14, abs_tol=0.1))
    assert (math.isclose(obsv_prob[24][vocab["amethyst"]], 0.14, abs_tol=0.1))

    # 3rd colour slot
    assert (math.isclose(obsv_prob[25][vocab["empty"]], 0.82, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amaranth"]], 0.06, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amber"]], 0.06, abs_tol=0.1))
    assert (math.isclose(obsv_prob[25][vocab["amethyst"]], 0.06, abs_tol=0.1))

    # 4th colour slot
    assert (math.isclose(obsv_prob[26][vocab["empty"]], 0.96, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amaranth"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amber"]], 0.01, abs_tol=0.1))
    assert (math.isclose(obsv_prob[26][vocab["amethyst"]], 0.01, abs_tol=0.1))


def test_test_colour_set():
    vocab = TidyBlock.vocab
    RUN_COUNT = 1000
    OBSV_LEN = 27

    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="language", colourset="test")

    obsv = env.reset()


    assert ((obsv == vocab["maroon"]).any())
    assert ((obsv == vocab["mauve"]).any())
    assert ((obsv == vocab["navy blue"]).any())

    assert (~(obsv == vocab["amaranth"]).all())
    assert (~(obsv == vocab["amber"]).all())
    assert (~(obsv == vocab["amethyst"]).all())

def test_train_colour_sample():
    vocab = TidyBlock.vocab
    RUN_COUNT = 1000
    OBSV_LEN = 27

    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, colour_sample_size=48, block_sizes="big and small",
                    state_representation="vector", action_representation="language", colourset="train")

    obsv = env.reset()
    assert (((obsv >= vocab["amaranth"]) & (obsv <= vocab["magenta rose"])).any())
    assert (~(((obsv >= vocab["maroon"]) & (obsv <= vocab["yellow"])).all()))

def test_test_colour_sample():
    vocab = TidyBlock.vocab
    RUN_COUNT = 1000
    OBSV_LEN = 27

    env = TidyBlock(num_env=2, max_colours=3, min_colours=3, max_space_in_box=4, colour_sample_size=48, block_sizes="big and small",
                    state_representation="vector", action_representation="language", colourset="test")

    obsv = env.reset()
    assert ((((obsv >= vocab["maroon"]) & (obsv <= vocab["yellow"])).any()))
    assert (~((obsv >= vocab["amaranth"]) & (obsv <= vocab["magenta rose"])).all())

def test_discrete_action():
    vocab = TidyBlock.vocab
    env = TidyBlock(num_env=2, max_colours=4, min_colours=3, max_space_in_box=4, block_sizes="big and small",
                    state_representation="vector", action_representation="discrete")
    env.box_colours = torch.tensor([[vocab["amaranth"], vocab["amber"], vocab["amethyst"], vocab["apricot"]],
                                    [vocab["amaranth"], vocab["amber"], vocab["amethyst"], vocab["apricot"]]],
                                   dtype=torch.long)
    assert (env.discrete_action_space == 256)
    action_verb, action_block_size, action_block_colour = [], [], []
    action_start_box_colour, action_end_box_colour = [], []
    for i in range(256):
        action = env._select_discrete_action(i * torch.ones(2, dtype=torch.long))
        action_verb.append(action[0][1].item())
        action_block_size.append(action[1][1].item())
        action_block_colour.append(action[2][1].item())
        action_start_box_colour.append(action[3][1].item())
        action_end_box_colour.append(action[4][1].item())

    env_combinations = list(zip(action_verb, action_block_size, action_block_colour,
                                action_start_box_colour, action_end_box_colour))
    action_verb_tokens = [env.vocab["lift"], env.vocab["push"]]
    action_block_size_tokens = [env.vocab["big"], env.vocab["small"]]
    action_block_colour_tokens = [env.vocab["amaranth"], env.vocab["amber"],
                                  env.vocab["amethyst"], env.vocab["apricot"]]
    action_start_box_colour_tokens = [env.vocab["amaranth"], env.vocab["amber"],
                                      env.vocab["amethyst"], env.vocab["apricot"]]
    action_end_box_colour_tokens = [env.vocab["amaranth"], env.vocab["amber"],
                                    env.vocab["amethyst"], env.vocab["apricot"]]

    action_space = (len(action_verb_tokens) * len(action_block_size_tokens) * len(action_block_colour_tokens)
                    * len(action_start_box_colour_tokens) * len(action_end_box_colour_tokens))
    assert (env.discrete_action_space == action_space)
    action_combinations = [(a, b, c, d, e) for a in action_verb_tokens for b in action_block_size_tokens
                           for c in action_block_colour_tokens for d in action_start_box_colour_tokens for e in
                           action_end_box_colour_tokens]

    for action_combination in action_combinations:
        assert (action_combination in env_combinations)
    for env_combination in env_combinations:
        assert (env_combination in action_combinations)
